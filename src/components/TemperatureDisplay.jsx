import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TemperatureDisplay extends Component {
    render(){
        const {
            avg,
            min,
            max
        } = this.props

        return <div className="temperature-display">
            <p className="temperature-display-avg">{
                avg
            }</p>
            <div className="temperature-display-row">
                <p>{
                    max
                }</p>
                <p className="temperature-display-row-item--min">{
                    min
                }</p>
            </div>
        </div>
    }
}

TemperatureDisplay.propTypes = {
    avg: PropTypes.number.isRequired,
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired
}

export default TemperatureDisplay
